public class SortUtil {


	/**
	 * Обобщенный метод для сортировки
	 * Обобщение позволяет не писать много перегруженных методов а выполнять общие операции для разных типов
	 * с наложенными ограничениями
	 * <p>
	 * В данном случае мы берем обобщение T и указываем что то что мы передаем в параметр должно ИМПЛЕМЕНТИРОВАТЬ
	 * ИНТЕРФЕЙС Comparable что бы мы могли сравнивать объекты
	 */
	public static <T extends Comparable<T>> void heapSort(T[] array) {
		// выстраиваем двоичную кучу
		for (int i = array.length / 2; i >= 0; i--) {
			heapify(array, i, array.length);
		}

		// выстраиваем массив
		// по свойству кучи берем максимум и ставим его на последнюю позицию в массиве и так далее
		for (int i = array.length - 1, j = 1; i >= 0; i--, j++) {
			swap(array, i, 0);
			heapify(array, 0, array.length - j);
		}

	}


	/**
	 * Метод востановления кучи
	 * работает по принципу
	 * 		4
	 * 	5		10
	 * 	изначально больший потомок равен - index; левый потомок	- leftChild = 2 * index + 1;
	 *  правый потомок rightChild = 2 * index + 2;
	 *
	 *  далее находит индекс максимума из array(left) array(index) array (right)
	 *  потом производит обмен элемента с максимальным индексом и корня
	 *  операция повторяется пока либо не достигнет листа либо не востоновит порядок кучи
	 */
	private static <T extends Comparable<T>> void heapify(T[] array, int index, int size) {
		while (true) {
			int leftChild = 2 * index + 1;
			int rightChild = 2 * index + 2;
			int largestChild = index;

			if (leftChild < size && array[leftChild].compareTo(array[largestChild]) > 0) {
				largestChild = leftChild;
			}

			if (rightChild < size && array[rightChild].compareTo(array[largestChild]) > 0) {
				largestChild = rightChild;
			}

			if (largestChild == index) {
				break;
			}

			swap(array, index, largestChild);
			index = largestChild;
		}
	}


	/**
	 * Обмен двух элементов
	 */
	private static <T> void swap(T[] array, int i, int j) {
		T tmp = array[i];
		array[i] = array[j];
		array[j] = tmp;
	}
}
