/**
 * Класс для точки, который ИМПЛЕМЕНТИРУЕТ ИНТЕРФЕЙС  Comparable С ТИПОМ Point
 * ЭТО НУЖНО ДЛЯ ТОГО, ЧТО БЫ КОРРЕКТНО РАБОТАЛ МЕТОД Arrays.sort() ИНТЕРФЕЙС ПОЗВОЛЯЕТ СРАВНИВАТЬ
 * ДВА ОБЬЕКТА МЕТОДОМ compareTo() КОТОРЫЙ МЫ РЕАЛИЗУЕМ В ДАННОМ КЛАССЕ
 */
public class Point implements Comparable<Point> {
	// Координаты точки
	private double x, y;

	// Расстояние от (0;0) до (x;y)
	private double r;


	public Point(double x, double y) {
		// Имя поля и параметра совпадают this это обьект текущего класса, в котором мы обращаемся к полю и приравниваем
		// поле = параметр
		this.x = x;
		this.y = y;
		updateMagnitude();
	}

	// Вычислить расстояние от нк до точки, метод не публичный
	private void updateMagnitude() {
		r = Math.sqrt(x * x + y * y);
	}

	//
	public double sqrMagnitude() {
		return r;
	}

	/**
	 * Гетеры и сеттеры для х, у
	 */
	public double getX() {
		return x;
	}

	/**
	 * Сеттер для х, см конструктор принцип такой же
	 * после приравнивания обновляем расстояние
	 */
	public void setX(double x) {
		this.x = x;
		updateMagnitude();
	}

	public double getY() {
		return y;
	}

	/**
	 * аналогично х
	 */
	public void setY(double y) {
		this.y = y;
		updateMagnitude();
	}

	/**
	 * Метод сравнения двух обьектов возвращает 1 - если обьект больше чем то что в параметре
	 * 0 - если обьекты равны
	 * -1 - если обьект меньше обьекта в параметре
	 */
	@Override
	public int compareTo(Point o) {
		return Double.compare(this.sqrMagnitude(), o.sqrMagnitude());
	}

	@Override
	public String toString() {
		return String.format("(%f;%f) -> r = %f", x, y, r);
	}
}
