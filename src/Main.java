import java.util.Arrays;
import java.util.Locale;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Locale.setDefault(Locale.ROOT);
		Scanner in = new Scanner(System.in);
		solve(in);
	}

	private static void solve(Scanner in) {
		int s = readInt(in, "Введите количество точек:");
		Point[] points = readArray(in, s);

		int n = readInt(in, "Введите n:");

		System.out.println("Использовать пирамидальную сортировку? (true or false)");
		boolean isHeapSort = in.nextBoolean();

		if (isHeapSort) {
			SortUtil.heapSort(points);
		} else {
			Arrays.sort(points);
		}

		int pointsToPrint = Math.min(n, s);
		System.out.printf("%d ближайших точек:\n", pointsToPrint);
		for (int i = 0; i < pointsToPrint; i++) {
			System.out.println(points[i]);
		}
	}


	private static Point[] readArray(Scanner in, int s) {
		Point[] points = new Point[s];
		System.out.printf("Введите %d точек\n", s);
		for (int i = 0; i < s; i++) {
			points[i] = readPoint(in);
		}
		return points;
	}

	private static int readInt(Scanner in, String message) {
		System.out.println(message);
		return in.nextInt();
	}


	public static Point readPoint(Scanner in) {
		double x = readDouble(in, "Введите x:");
		double y = readDouble(in, "Введите y:");
		return new Point(x, y);
	}

	private static double readDouble(Scanner in, String message) {
		System.out.println(message);
		return in.nextDouble();
	}
}
